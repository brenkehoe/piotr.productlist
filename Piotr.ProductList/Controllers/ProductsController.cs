﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Piotr.ProductList.Data;
using Piotr.ProductList.Model;

namespace Piotr.ProductList.Controllers
{
    public class ProductsController : ApiController
    {
        public static IRepository<Product> ExpenseRepository
            = new InMemoryRepository<Product>();

        public IEnumerable<Product> Get()
        {
            return ExpenseRepository.Items.ToArray();
        }

        public Product Get(Guid id)
        {
            Product entity = ExpenseRepository.Get(id);
            if (entity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return entity;
        }

        public HttpResponseMessage Post(Product value)
        {
            var result = ExpenseRepository.Add(value);
            if (result == null)
            {
                // the entity with this key already exists
                throw new HttpResponseException(HttpStatusCode.Conflict);
            }
            var response = Request.CreateResponse<Product>(HttpStatusCode.Created, value);
            string uri = Url.Link("DefaultApi", new { id = value.Id });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        public HttpResponseMessage Put(Guid id, Product value)
        {
            value.Id = id;
            var result = ExpenseRepository.Update(value);
            if (result == null)
            {
                // entity does not exist
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Delete(Guid id)
        {
            var result = ExpenseRepository.Delete(id);
            if (result == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}